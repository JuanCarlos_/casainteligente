package edu.mecatronica.casainteligente;

import java.util.Timer;
import java.util.TimerTask;

public class Carga {
	private boolean estado;

	public Carga() {
		this.estado = false;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	public void setEstado(final boolean estado, long tiempo){
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				setEstado(estado);
			}
		};
		Timer timer = new Timer();
		timer.schedule(task, tiempo);
	}

}
