package edu.mecatronica.casainteligente;

import java.util.HashMap;

import edu.mecatronica.casainteligente.data.IdCarga;

public class Casa extends HashMap<IdCarga, Carga> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Casa() {
		put(IdCarga.CARGA1, new Carga());
		put(IdCarga.CARGA2, new Carga());
		put(IdCarga.CARGA3, new Carga());
		put(IdCarga.CARGA4, new Carga());
		put(IdCarga.CARGA5, new Carga());
		put(IdCarga.CARGA6, new Carga());
		put(IdCarga.CARGA7, new Carga());
		put(IdCarga.CARGA8, new Carga());
	}
}
