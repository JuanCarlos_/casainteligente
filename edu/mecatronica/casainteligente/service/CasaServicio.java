package edu.mecatronica.casainteligente.service;

import edu.mecatronica.casainteligente.Casa;
import edu.mecatronica.casainteligente.data.IdCarga;

public class CasaServicio {

	public static boolean estaPrendida(IdCarga idcarga, Casa casa) {
		return casa.get(idcarga).isEstado();
	}

	public static void encender(IdCarga idcarga, Casa casa) {
		if (!estaPrendida(idcarga, casa)) {
			casa.get(idcarga).setEstado(true);
		}
	}

	public static void apagar(IdCarga idcarga, Casa casa) {
		if (estaPrendida(idcarga, casa)) {
			casa.get(idcarga).setEstado(false);
		}
	}
	
	public static void encender(IdCarga idcarga, Casa casa, long tiempo) {
		if (!estaPrendida(idcarga, casa)) {
			casa.get(idcarga).setEstado(true, tiempo);
		}
	}

	public static void apagar(IdCarga idcarga, Casa casa, long tiempo) {
		if (estaPrendida(idcarga, casa)) {
			casa.get(idcarga).setEstado(false, tiempo);
		}
	}
}
